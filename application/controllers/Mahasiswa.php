<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mahasiswa_model');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['mahasiswas'] = $this->Mahasiswa_model->get_mahasiswa_with_hobi();
		$this->load->view('mahasiswa/index', $data);
	}

	public function create()
	{
		$data['hobi'] = $this->Mahasiswa_model->ref_hobi();
		$this->load->view('mahasiswa/create', $data);
	}

	public function store()
	{
		$this->db->trans_start();

		$data['mahasiswa'] = array(
			'nim' => $this->input->post('nim'),
			'nama' => $this->input->post('nama'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'alamat' => $this->input->post('alamat')
		);

		$data['hobi'] = $this->input->post('hobi');
		$trans_status = $this->Mahasiswa_model->create($data);

		if ($trans_status) {
			$this->db->trans_complete();
			redirect('mahasiswa');
		} else {
			$this->db->trans_rollback();
			$this->load->view('mahasiswa/create');
		}
	}

	public function edit($id)
	{
		$data['mahasiswa'] = $this->Mahasiswa_model->get_by_id($id);
		$data['hobi'] = $this->Mahasiswa_model->ref_hobi();
		$data['mahasiswa_hobi'] = $this->Mahasiswa_model->get_mahasiswa_hobi($id);
		$this->load->view('mahasiswa/edit', $data);
	}

	public function update($id)
	{
		$this->db->trans_start();

		$data['mahasiswa'] = array(
			'nim' => $this->input->post('nim'),
			'nama' => $this->input->post('nama'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'alamat' => $this->input->post('alamat')
		);

		$data['hobi'] = $this->input->post('hobi');
		$trans_status = $this->Mahasiswa_model->update($id, $data);

		if ($trans_status) {
			$this->db->trans_complete();
			redirect('mahasiswa');
		} else {
			$this->db->trans_rollback();
			$this->load->view('mahasiswa/edit', $data);
		}
	}

	public function delete($id)
	{
		$this->db->trans_start();

		$this->Mahasiswa_model->delete($id);

		$this->db->trans_complete();

		redirect('mahasiswa');
	}
}
