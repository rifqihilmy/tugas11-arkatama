<!DOCTYPE html>
<html>

<head>
	<title>Daftar Mahasiswa</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>

<body>
	<div class="container">
		<h1 class="text-center">Daftar Mahasiswa</h1>
		<a href="<?php echo site_url('mahasiswa/create'); ?>" class="btn btn-success">Tambah Mahasiswa</a>
		<?php if (empty($mahasiswas)) { ?>
			<p>Tidak ada data mahasiswa.</p>
		<?php } else { ?>
			<table class="table">
				<thead>
					<tr>
						<th>NIM</th>
						<th>Nama</th>
						<th>Jenis Kelamin</th>
						<th>Alamat</th>
						<th>Hobi</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($mahasiswas as $mahasiswa) : ?>
						<tr>
							<td><?php echo $mahasiswa['nim']; ?></td>
							<td><?php echo $mahasiswa['nama']; ?></td>
							<td><?php echo $mahasiswa['jenis_kelamin']; ?></td>
							<td><?php echo $mahasiswa['alamat']; ?></td>
							<td>
								<?php if (!empty($mahasiswa['daftar_hobi'])) {
									echo $mahasiswa['daftar_hobi'];
								} else {
									echo "Tidak ada hobi";
								} ?>
							</td>
							<td>
								<a href="<?php echo site_url('mahasiswa/edit/' . $mahasiswa['id']); ?>" class="btn btn-info">Edit</a>
								<a href="<?php echo site_url('mahasiswa/delete/' . $mahasiswa['id']); ?>" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus mahasiswa ini?')">Hapus</a>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php } ?>
	</div>



</body>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</html>